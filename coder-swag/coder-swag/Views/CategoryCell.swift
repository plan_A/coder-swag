//
//  CategoryCell.swift
//  coder-swag
//
//  Created by Jenia on 6/6/18.
//  Copyright © 2018 Jenia. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {
    
    @IBOutlet weak var CategoryImage:UIImageView!
    @IBOutlet weak var CategoryTitle:UILabel!

    func updateViews(category:Category){
        CategoryImage.image = UIImage(named: category.imageName)
        CategoryTitle.text = category.title
    }

}
