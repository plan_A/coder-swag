//
//  ProductCell.swift
//  coder-swag
//
//  Created by Jenia on 6/6/18.
//  Copyright © 2018 Jenia. All rights reserved.
//

import UIKit

class ProductCell: UICollectionViewCell {
    @IBOutlet weak var ProductImage: UIImageView!
    @IBOutlet weak var ProductTitle: UILabel!
    @IBOutlet weak var ProductPrice: UILabel!
    
    func updateViews (product: Product) {
        ProductImage.image = UIImage(named: product.imageName)
        ProductTitle.text = product.title
        ProductPrice.text = product.price
    }
}
