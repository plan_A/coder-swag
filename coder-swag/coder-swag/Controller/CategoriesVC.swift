//
//  ViewController.swift
//  coder-swag
//
//  Created by Jenia on 6/3/18.
//  Copyright © 2018 Jenia. All rights reserved.
//

import UIKit

class CategoriesVC: UIViewController, UITableViewDataSource,
UITableViewDelegate {
    
    
    @IBOutlet weak var categoryTable:UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        categoryTable.dataSource = self
        categoryTable.delegate = self
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataService.instance.getCategories().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell") as? CategoryCell{
            let category = DataService.instance.getCategories()[indexPath.row]
            cell.updateViews(category: category)
            //print (indexPath.row)
            return cell
            
        }
        else {
            return CategoryCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let category = DataService.instance.getCategories()[indexPath.row]
        performSegue(withIdentifier: "ProductsVC", sender: category)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
            if let ProductsVC = segue.destination as? ProductsVC {
                let BarBtn = UIBarButtonItem()
                BarBtn.title = ""
                navigationItem.backBarButtonItem = BarBtn
                assert(sender as? Category != nil)
                ProductsVC.initProducts(category: sender as! Category)
        
           
        }
    }
}

